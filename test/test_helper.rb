ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def log_in_as(user, options = {})
  	password		= options[:password] 		|| 'password'
  	remember_me	= options[:remember_me] || '1'

  	if integration_test?
  		post login_path, session: { email: 				user.email,
  																password: 		password,
  																remember_me: 	remember_me }
  	else
  		session[:user_id] = user.id
  	end

  end

  # Returns true if the test user is logged in
  def is_logged_in?
  	!session[:user_id].nil?
  end

  private
  	def integration_test?
  		# checks if 'post_via_redirect' is defined
  		# that method is defined in integration tests only
  		defined?(post_via_redirect)
  	end

end

class Profile < ActiveRecord::Base
  
  # attribute accessors
  attr_accessor :diseases_array

  # associations
  belongs_to :user

  has_many   :profile_diseases, class_name:   "ProfileDisease",
                                foreign_key:  "profile_id",
                                dependent:    :destroy

  has_many   :diseases, through: :profile_diseases, source: :disease

  # validations
  validates :user_id, presence: true
  validates :address, length: { maximum: 255 }
  validates :gender, 
  					:inclusion => { :in => :gender_list }

  validates :age,
  					:numericality => { only_integer: true,
  													 greater_than: 0 }

  validate  :profile_exists_for_user?

  # callbacks
  before_save 	:address_uppercase

  before_create :default_disease


  # list of possible genders (public)
	def gender_list
  		 ['Male', 'Female', 'Alien', 'Other'] 		
  end

  def add_disease(disease)
    ProfileDisease.create(profile: self, disease: disease)
  end

  private

  	def address_uppercase
  		self.address = address.nil? ? "NO ADDRESS" : address.upcase
  	end
 		
 		def profile_exists_for_user?
 			if Profile.find_by(user: user)
 				errors.add(:user, "Profile already exists for User " + user.email)
 			end
 		end

    def default_disease
      debugger
      if :diseases_array.nil? 
        Disease.create(name: "selfishness", 
                       lethal: false,
                       days_left: 1000, 
                       profile: self.id)
      end 
    end

end

class SessionsController < ApplicationController
  
  def new
  end

  def create
  	@user = User.find_by(email: params[:session][:email].downcase)
  	
  	# auth OK
  	if @user && @user.authenticate(params[:session][:password])

      # user activated?
      if @user.activated?
        log_in @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        redirect_back_or @user
      else
        message  = "Account not activated. "
        message += "Check your email =D"
        flash[:warning] = message
        redirect_to root_url
      end
  		
  	else
  		# error message
  		flash.now[:danger] = 'Invalid email/password'
	  	render 'new'
	  end

  end

  def destroy

    # logs out the user calling the method in SessionsHelper
    log_out if logged_in?

    # redirect to root
    redirect_to root_url    
    
  end

end

class ReloadProfileDiseasesIndex < ActiveRecord::Migration
  def change
  	remove_index :profile_diseases, [:profile_id, :disease_id]
  	add_index :profile_diseases, [:profile_id, :disease_id], unique: true
  end
end

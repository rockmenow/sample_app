class AddIndexToProfileDisease < ActiveRecord::Migration
  def change
  	add_index :profile_diseases, [:profile_id, :disease_id], :unique
  end
end

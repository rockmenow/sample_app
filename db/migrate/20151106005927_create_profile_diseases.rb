class CreateProfileDiseases < ActiveRecord::Migration
  def change
    create_table :profile_diseases do |t|
      t.references :profile, index: true, foreign_key: true
      t.references :disease, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

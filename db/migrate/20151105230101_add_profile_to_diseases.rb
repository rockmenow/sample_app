class AddProfileToDiseases < ActiveRecord::Migration
  def change
  	add_reference :diseases, :profile, index: true, foreign_key: true
  end
end

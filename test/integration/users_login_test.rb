require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@user = users(:vitor)
  end

  test "login with invalid input" do

  	# go to the login page
		get login_path

		# login template (sessions/new)
		assert_template 'sessions/new'

		# send a wrong HTTP POST request to /login
		post login_path, session: { email: "", 
																password: "" }

		# it's still in the same page
		assert_template 'sessions/new'

		# flash object is here
		assert_not flash.empty?

		# go to home page
		get root_path

		# flash should NOT appear anymore
		assert flash.empty?

  end

  test "login with valid input" do

  	# go to the login page
  	get login_path

  	# login template (sessions/new)
		assert_template 'sessions/new'

		# send a correct HTTP POST to /login
		# @user is defined in setup
  	post login_path, session: { email: @user.email, password: 'password' }

  	# redirect to /user/{ID}
  	assert_redirected_to @user

  	# follow the redirect
  	follow_redirect!

  	# user/show template
  	assert_template 'users/show'

  	# login should not be shown
  	assert_select "a[href=?]", login_path, count: 0

  	# logout should
  	assert_select "a[href=?]", logout_path
  	assert_select "a[href=?]", user_path(@user)

  end

  test "login with valid input followed by logout" do

    # go to the login page
    get login_path

    # login template (sessions/new)
    assert_template 'sessions/new'

    # send a correct HTTP POST to /login
    # @user is defined in setup
    post login_path, session: { email: @user.email, password: 'password' }

    # user should be logged in
    assert is_logged_in?

    # redirect to /user/{ID}
    assert_redirected_to @user

    # follow the redirect
    follow_redirect!

    # user/show template
    assert_template 'users/show'

    # login should not be visible
    assert_select "a[href=?]", login_path, count: 0

    # logout should
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)

    # logout action
    delete logout_path

    # user should not be logged in anymore
    assert_not is_logged_in?

    # after log out redirects to root_url
    assert_redirected_to root_url

    # simulate a user clicking logout in a second window
    delete logout_path

    # follow the redirect
    follow_redirect!

    # check for elements (opposite of the above)
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0

  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')

    # get instance variable with assigns[]
    assert_equal cookies['remember_token'], assigns[:user].remember_token
    #assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end


end

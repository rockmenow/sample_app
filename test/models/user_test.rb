require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@user = User.new(name: "dummy", email: "dummy@dummy.com",
  										password: "blabla", password_confirmation: "blabla")  	
  end

  test "should be valid" do
		assert @user.valid?  	
  end

  test "name should be present" do
  	@user.name = " "
  	assert_not @user.valid?
  end

  test "email should be present" do
  	@user.email = " "
  	assert_not @user.valid?
  end

  test "name should not be too long" do
  	@user.name = "a" * 51
  	assert_not @user.valid?
  end

  test "email should not be too long" do
  	@user.name = "a" * 244 + "@example.com"
  	assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do

  	valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]

    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end

  end

  test "email validation should reject valid addresses" do

  	valid_addresses = %w[foo@foo..foo user@example,com USER_at_foo.COM A_USfoo@]

    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert_not @user.valid?, "#{valid_address.inspect} should be invalid"
    end

  end

  test "email addresses should be unique" do
  	duplicate_user = @user.dup
  	duplicate_user.email = @user.email.upcase
  	@user.save
  	assert_not duplicate_user.valid?
  end

  test "password length should have a minimum length" do
  	@user.password = @user.password_confirmation = "a" * 5
  	assert_not @user.valid?
  end

  test "password should not be blank" do
  	@user.password = @user.password_confirmation = " " * 6
  	assert_not @user.valid?
  end

  test "email should be stored as lowercase" do
  	mixedcase_email = "Vitor@tesT.coM"
  	@user.email = mixedcase_email
  	@user.save
  	@user.reload
  	assert_equal mixedcase_email.downcase, @user.email
  end

  test "authenticated?" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    vitor = users(:vitor)
    rockmenow  = users(:rockmenow)
    assert_not vitor.following?(rockmenow)
    vitor.follow(rockmenow)
    assert vitor.following?(rockmenow)
    assert rockmenow.followers.include?(vitor)
    vitor.unfollow(rockmenow)
    assert_not vitor.following?(rockmenow)
  end

end

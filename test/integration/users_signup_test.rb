require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do

	  # get signup page
	  get signup_path

	  # compare User.count before and after the HTTP POST 
	  assert_no_difference 'User.count' do

	  	post users_path, user: { name: "",
	  													email: "user@invalid",
	  													password: "foo",
	  													password_confirmation: "bar" }
	  end

	  assert_template 'users/new'
	  assert_select 'div#error_explanation'
	  assert_select 'div.alert-danger'

	end


	test "valid signup information" do

		# get signup page
		get signup_path

		# compare User.count before and after the HTTP POST 
	  assert_difference 'User.count', 1 do

	  	post_via_redirect users_path, user: { name: "example",
	  													email: "example@example.com",
	  													password: "blabla",
	  													password_confirmation: "blabla" }
	  end

	  # the user is redirected to the profile
	  #assert_template 'users/show'

	  # flash message shows up
	  #assert_select 'div.alert-success'

	  # success
	  #assert flash[:success]

	  # user was logged in automatically
	  #assert is_logged_in?

	end

	test "valid signup information with account activation" do
	  get signup_path
	  assert_difference 'User.count', 1 do
	    post users_path, user: { name:  "Example User",
	                             email: "user@example.com",
	                             password:              "password",
	                             password_confirmation: "password" }
	  end

	  assert_equal 1, ActionMailer::Base.deliveries.size
	  user = assigns(:user)
	  assert_not user.activated?
	  # Try to log in before activation.
	  log_in_as(user)
	  assert_not is_logged_in?
	  # Invalid activation token
	  get edit_account_activation_path("invalid token")
	  assert_not is_logged_in?
	  # Valid token, wrong email
	  get edit_account_activation_path(user.activation_token, email: 'wrong')
	  assert_not is_logged_in?
	  # Valid activation token
	  get edit_account_activation_path(user.activation_token, email: user.email)
	  assert user.reload.activated?
	  follow_redirect!
	  assert_template 'users/show'
	  assert is_logged_in?
	  
  end

end

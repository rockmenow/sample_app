class CreateDiseases < ActiveRecord::Migration
  def change
    create_table :diseases do |t|
      t.string :name
      t.boolean :lethal

      t.timestamps null: false
    end
  end
end

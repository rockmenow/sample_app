class ProfileDisease < ActiveRecord::Base
  belongs_to :profile
  belongs_to :disease
end

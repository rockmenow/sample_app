class User < ActiveRecord::Base
	
	attr_accessor :remember_token,
								:activation_token,
								:reset_token

	# association with microposts
	has_many :microposts, dependent: :destroy

	has_many :active_relationships, class_name: 	"Relationship",
																	foreign_key: 	"follower_id",
																	dependent: 		:destroy

	has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  has_one  :profile, dependent: :destroy
														

	# downcase_email callback is called before saving user on db
	# (create/update)
	before_save 	:downcase_email

	# this one is called before creating the user
	# (create only)
	before_create :create_activation_digest

	# name
	validates :name, 	presence: true, length: { maximum: 50 }

	# email
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email,
							presence: true, 
							length: { maximum: 255 }, 
							format: { with: VALID_EMAIL_REGEX },
							uniqueness: { case_sensitive: false }

	# ?
	has_secure_password

	# password
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

	# methods inside this block are 'class' methods
	# should be used as 'User.digest' and 'User.new_token'
	# * 'self' in this context is User class
	class << self

		# retuns hash digest of the string
		def digest(string)
			cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
																										BCrypt::Engine.cost
			BCrypt::Password.create(string, cost: cost)
		end

		# returns a random token
		def new_token
			SecureRandom.urlsafe_base64
		end

	end

	def remember
		# * 'self' in this context is a User object instance
		self.remember_token = User.new_token
		update_attribute(:remember_digest, User.digest(remember_token))
	end

	def forget
		update_attribute(:remember_digest, nil)
	end

	def authenticated?(attribute, token)

		# this can be either activation_digest or remember_digest
		# 'send' is used to create the proper attribute
		digest = send("#{attribute}_digest")
		return false if digest.nil?
		BCrypt::Password.new(digest).is_password?(token)
	end

	def activate
		update_attribute(:activated, 		true)
		update_attribute(:activated_at, Time.zone.now)
	end

	def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def create_reset_digest
  	self.reset_token = User.new_token
  	update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def password_reset_expired?
  	reset_sent_at < 2.hours.ago
  end

  def feed
  	Micropost.where("user_id = ?", id)
  end

  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

	private

		def downcase_email
			self.email = email.downcase
		end

		def create_activation_digest
			self.activation_token 	= User.new_token
			self.activation_digest 	= User.digest(activation_token)
		end

end
